var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var nodemon = require('gulp-nodemon');

gulp.task('sass', function () {
	return gulp.src('public/scss/*.scss')
	.pipe(sass({outputStyle: 'compressed', sourceComments: 'map'}, {errLogToConsole: true}))
	.pipe(prefix("last 2 versions"))
	.pipe(gulp.dest('public/css'))
	.pipe(reload({stream:true}));
});

// gulp.task('browser-sync', ['nodemon'], function() {
// 	browserSync.init(null, {
// 		proxy: {
// 			host: "http://localhost",
// 			port: "3000"
// 		}
// 	});
// });

gulp.task('browser-sync', ['nodemon'], function() {
	browserSync.init(null, {
		proxy: "http://localhost:3000",
        files: ["public/**/*.*"],
        browser: "firefox",
        port: 7000,
	});
});

gulp.task('default', ['sass', 'browser-sync'], function () {
	gulp.watch("public/scss/*.scss", ['sass']);
	gulp.watch(["public/js/**/*.js", "./*.html"], reload);
});

gulp.task('nodemon', function (cb) {
	var called = false;
	return nodemon({script: 'server.js'}).on('start', function () {
		if (!called) {
			called = true;
			cb();
		}
	});
});